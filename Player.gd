extends KinematicBody2D

enum State {WALK, DASH}
var move_state = State.WALK

const ACCEL_WALK : int = 100 #pixels / second
const ACCEL_DASH : int = 200
const FRICTION : int = 15
const MAX_WALK_SPEED : int = 300
const MAX_DASH_SPEED : int = 500

var velocity := Vector2.ZERO
var facing := Vector2.ZERO #The vector between the player's position and the mouse position, normalized
var dash_timer : float = .5

var keyboard_input : bool = true
func _input(event: InputEvent) -> void:
	facing = get_local_mouse_position().normalized()
	if(event is InputEventJoypadButton) or (event is InputEventJoypadMotion):
		keyboard_input = false
	else:
		keyboard_input = true

func _physics_process(delta) -> void:
	facing = get_local_mouse_position().normalized()
	if Input.is_action_pressed("move_dash") and move_state == State.WALK:
		self.move_state = State.DASH
		
	match move_state:
		State.WALK:
			var input_vector : Vector2 = Vector2.ZERO
			input_vector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
			input_vector.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
			if keyboard_input: # normalizes the input vector such that diagonals arent faster if using
				#WASD rather than a joycon
				input_vector = input_vector.normalized()
			if input_vector != Vector2.ZERO:
				velocity += input_vector * ACCEL_WALK * delta
				velocity = velocity.clamped(MAX_WALK_SPEED*delta)
			else:
				velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
		State.DASH:
			velocity += facing * ACCEL_DASH * delta
			velocity = velocity.clamped(MAX_DASH_SPEED*delta)
			dash_timer -= delta
			if dash_timer <= 0:
				move_state = State.WALK
		_:
			pass
	move_and_collide(velocity)

func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
